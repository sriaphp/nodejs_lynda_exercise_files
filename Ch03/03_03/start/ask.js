var questions = ['What is your name?','What is your fav hobby?','What is your date of birth?'];
var answers = [];
function ask(i) {
	process.stdout.write(`\n\n\n ${questions[i]}`);
};

process.stdin.on('data',function(data) {
	//process.stdout.write('\n'+data.toString().trim()+'\n');
	answers.push(data.toString().trim());
	if(answers.length < questions.length) {
		ask(answers.length);
	} else {
		process.exit();
	}
});

process.on('exit',function() {
	process.stdout.write("\n\n\n\n\n All questions Answered \n\n\n\n\n ");
});

ask(0);