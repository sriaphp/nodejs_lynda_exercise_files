
/* var events = require('events');
var emitter = new events.EventEmitter();

emitter.on('customEvent',function(message,status){
    console.log(`${status}: ${message}`);
});
//We can trigger or emit a custom event with the emit function
emitter.emit('customEvent',"Hello World","200");*/

var EventEmitter = require('events').EventEmitter; //This will pull EventEmitter constructor out of event
var util = require('util');
//We want the person object to inherit the eventemitter
var Person = function(name) {
    this.name = name;
}

//The utilities module has an inherits function, and it's a way that we can add a object to the prototype of an existing object. That's how JavaScript handles inheritance
util.inherits(Person,EventEmitter);

var ben = new Person("Ben Franklin");

ben.on('speak',function(said){
 console.log(`${this.name}: ${said}`);
});

ben.emit('speak','You may delay, but time will not');

//The EventEmitter provides us a way to create custom objects that raise custom events that can be handled asynchronously
//The EventEmitter provides us a way to create custom objects that raise custom events that can be handled asynchronously.And because the events are handled asynchronously, it is a very important tool in node.js