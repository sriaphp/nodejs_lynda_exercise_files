var readline = require('readline');
var rl = readline.createInterface(process.stdin,process.stdout);

var realPerson = {
    name: '',
    sayings: []
};

rl.question('What is your name?', function(answer) {
    realPerson.name = answer;
    rl.setPrompt(`What would ${realPerson.name} say?`);
    rl.prompt();
    rl.on('line',function(saying) {
        realPerson.sayings.push(saying);
        if(saying.toLowerCase().trim() === 'exit') {
            rl.close();
        } else {
            rl.setPrompt(`What else would ${realPerson.name} say? (Exit on leave)`);
            rl.prompt();
        }
    });
});

rl.on('close',function() {
 console.log("%s is real person that say %j",realPerson.name,realPerson.sayings);
    process.exit();
});